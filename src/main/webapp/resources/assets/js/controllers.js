angular.module("GeoWeatherApp")
    .controller('PageCtrl', ['$http', 'LocalStorage', '$window', function ($http, LocalStorage, $window) {
        var self = this
        self.data = LocalStorage.getLastData();
        self.places = []
        self.location_msg = "Determining User Location....";
        self.location_retrieve_state = false;
        self.error = null;
        var lastEntered = LocalStorage.getLastEnteredLocation()
        if (lastEntered)
            self.added_loc = lastEntered
        else
            self.added_loc = {
                lat: 0.00,
                lng: 0.00
            }

        self.getWeatherData = function (lat, lng, callback) {
            self.error = null
            self.data = {}
            var url = 'http://api.geonames.org/findNearByWeatherJSON?lat=' + lat + '&lng=' + lng + '&username=prognotest'
            $http.get(url).then(function (response) {
                    if (response.data == null) {
                        self.error = 'NET ERROR: Empty Response!!!';
                        return
                    }
                    if (response.data.status) {
                        self.error = ('Server ERROR: ' + response.data.status.message)
                        return
                    }

                    self.data = response.data.weatherObservation;
                    console.log(response.data);
                    LocalStorage.setLastData(self.data)
                    if (callback)
                        callback(self.data);
                },
                function (err) {
                    self.error = 'NET ERROR ' + err.status + ": " + err.statusText
                });


        };
        self.addLocation = function (data) {
            self.places.unshift(
                {
                    lat: data.lat,
                    lng: data.lng,
                    title: (data.stationName + ", " + data.countryCode)
                }
            )
        }
        self.refreshWeatherData = function (place) {
            if(place)
            {
                self.getWeatherData(place.lat, place.lng)
            }
            else
            {
                LocalStorage.setLastEnteredLocation(self.added_loc);
                self.getWeatherData(self.added_loc.lat, self.added_loc.lng, self.addLocation)
            }



        };
        self.loadDefaults = function () {
            //Use Defaults
            var default_coord = {lat: 9, lng: 7, stationName: 'ABUJA', countryCode:'NG'}
            self.addLocation(default_coord);
            self.getWeatherData(default_coord.lat, default_coord.lng);
        }

        self.init = function () {
            if (self.data)
                self.addLocation(self.data)

            if (navigator.geolocation)
            {
                self.location_retrieve_state = true
                navigator.geolocation.getCurrentPosition(function (position) {
                    self.getWeatherData(position.coords.latitude, position.coords.longitude, self.addLocation);
                    self.location_retrieve_state = false;

                }, function (error)
                {
                    switch(error.code) {
                        case error.PERMISSION_DENIED:
                            self.error = "LOCATION ERR: User denied the request for Geolocation."
                            break;
                        case error.POSITION_UNAVAILABLE:
                            self.error = "LOCATION ERR: Location information is unavailable."
                            break;
                        case error.TIMEOUT:
                            self.error = " LOCATION ERR:The request to get user location timed out."
                            break;
                        case error.UNKNOWN_ERROR:
                            self.error = "LOCATION ERR: An unknown error occurred."
                            break;
                        default:
                        	self.error = error.message
                            	
                    }
                    self.location_retrieve_state = false;
                });
            }
            else {
                $window.alert(" LOCATION ERR: Geolocation is not supported by this browser.");
                self.loadDefaults()

            }


        }


        self.init();

    }]);