/**
 * Created by User on 16/01/2016.
 */
angular.module("GeoWeatherApp", ['ui.bootstrap', 'angular-loading-bar'])
.factory("LocalStorage", function($window, $rootScope) {
    return {
        setLastData: function(val) {
            $window.localStorage && $window.localStorage.setItem('last_data', JSON.stringify(val));
            return this;
        },
        getLastData: function() {
            return $window.localStorage && JSON.parse($window.localStorage.getItem('last_data'));
        },
        setLastEnteredLocation: function(val) {
            $window.localStorage && $window.localStorage.setItem('last_coord', JSON.stringify(val));
            return this;
        },
        getLastEnteredLocation: function() {
            return $window.localStorage && JSON.parse($window.localStorage.getItem('last_coord'));
        }
    };
});