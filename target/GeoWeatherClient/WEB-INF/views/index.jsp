<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="GeoWeatherApp">
<head >
<spring:url value="/resources/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapcss" />
<spring:url value="resources/css/animate.min.css"
	var="animatecss" />
<spring:url
	value="resources/angular-loading-bar/build/loading-bar.min.css"
	var="loadingbarcss" />
<spring:url value="resources/angularjs/angular.min.js"
	var="angularjs" />
<spring:url value="resources/js/ui-bootstrap.min.js"
	var="angularbootstrapjs" />
<spring:url
	value="resources/angular-loading-bar/build/loading-bar.min.js"
	var="loadingbarjs" />
<spring:url value="resources/js/app.js" var="appjs" />
<spring:url value="resources/js/controllers.js"
	var="controllersjs" />

<link href="${bootstrapcss}" rel="stylesheet">
<link href="${animatecss}" rel="stylesheet">
<link href="${loadingbarcss}" rel="stylesheet">
</head>
<body ng-controller="PageCtrl as ctrl" style="margin: 0 5%">
	<div align="right">
		<h1>
			<strong>{{ctrl.data.stationName ? ctrl.data.stationName :
				'-'}}</strong>
		</h1>

		<h3>{{ctrl.data.countryCode ? ctrl.data.countryCode : '-'}}</h3>
	</div>
	<div class="row">
		<div class="col-sm-3">
			<h3>Result History</h3>
			<ul class="list-group">
				<li class="list-group-item" ng-repeat="place in ctrl.places"><a
					ng-click="ctrl.refreshWeatherData(place)">{{place.title}}</a></li>
			</ul>

		</div>
		<div class="col-sm-9">

			<form class="form-inline" ng-submit="ctrl.refreshWeatherData()">
				<div class="form-group">
					<label class="sr-only" for="idLat">Add Latitude</label>

					<div class="input-group">
						<div class="input-group-addon">Lat:</div>
						<input type="text" class="form-control input-sm" id="idLat"
							placeholder="Latitude" ng-model="ctrl.added_loc.lat" required>

					</div>
					<label class="sr-only" for="idLng">Add Longitude</label>

					<div class="input-group">
						<div class="input-group-addon">Lng:</div>
						<input type="text" class="form-control input-sm" id="idLng"
							placeholder="Longitude" ng-model="ctrl.added_loc.lng" required>

					</div>
				</div>
				<button type="submit" class="btn btn-primary">Add Weather
					Info</button>


			</form>
			<h4 style="color: red; font-style: italic" ng-hide="!ctrl.error">{{ctrl.error}}</h4>
			<h4 class="animated bounce infinite"
				style="color: green; font-style: italic; text-align: right"
				ng-hide="!ctrl.location_retrieve_state">{{ctrl.location_msg}}</h4>
			<hr>
			<table class="table table-striped table-bordered">
				<thead>
					<th class="col-md-5"></th>
					<th class="col-md-7"></th>
				</thead>
				<tbody>
					<tr>
						<td>Temperature</td>
						<td>{{ctrl.data.temperature ? (ctrl.data.temperature + "
							&#176;C") : '-'}}</td>
					</tr>
					<tr>
						<td>Humidity</td>
						<td>{{ctrl.data.humidity ? ctrl.data.humidity : '-'}}</td>
					</tr>

					<tr>
						<td>Weather Condition</td>
						<td>{{ctrl.data.weatherCondition ? ctrl.data.weatherCondition
							: '-'}}</td>
					</tr>

					<tr>
						<td>Wind Speed</td>
						<td>{{ctrl.data.windSpeed ? (ctrl.data.windSpeed + ' Knots')
							: '-'}}</td>
					</tr>

					<tr>
						<td>Wind Direction</td>
						<td>{{ctrl.data.windDirection ? ctrl.data.windDirection :
							'-'}}</td>
					</tr>

					<tr>
						<td>Clouds</td>
						<td>{{ctrl.data.clouds ? ctrl.data.clouds : '-'}}
							({{ctrl.data.cloudsCode ? ctrl.data.cloudsCode : '-'}})</td>
					</tr>

					<tr>
						<td>Dew point</td>
						<td>{{ctrl.data.dewPoint ? ctrl.data.dewPoint : '-'}}</td>
					</tr>

					<tr>
						<td>Elevation</td>
						<td>{{ctrl.data.elevation ? (ctrl.data.elevation + 'm') :
							'-'}}</td>
					</tr>

					<tr>
						<td>HectoPascal Altimeter</td>
						<td>{{ctrl.data.hectoPascAltimeter ?
							ctrl.data.hectoPascAltimeter : '-'}}</td>
					</tr>

					<tr>
						<td>Latitude</td>
						<td>{{ctrl.data.lat ? ctrl.data.lat : '-'}}</td>
					</tr>
					<tr>
						<td>Longitude</td>
						<td>{{ctrl.data.lng ? ctrl.data.lng : '-'}}</td>
					</tr>

					<tr>
						<td>ICAO</td>
						<td>{{ctrl.data.ICAO ? ctrl.data.ICAO : '-'}}</td>
					</tr>

					<tr>
						<td>Observation</td>
						<td>{{ctrl.data.observation ? ctrl.data.observation : '-'}}</td>
					</tr>

					<tr>
						<td>Time Stamp</td>
						<td>{{ctrl.data.datetime ? ctrl.data.datetime : '-'}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<script src="${angularjs}"></script>
	<script src="${angularbootstrapjs}"></script>
	<script
		src="${loadingbarjs}"></script>
	<script src="${appjs}"></script>
	<script src="${controllersjs}"></script>
</body>
</html>